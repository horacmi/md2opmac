#!/usr/bin/env python3

# Michal Horacek, 2016

'''Simple python3 script used to convert from markdown or HTML input format to OPmac TeX format.

Script uses markdown2 library to parse input markdown text. Another option is to use extern
markdown parser and pass parsed HTML text as input.
'''

import sys
from argparse import ArgumentParser
from html.parser import HTMLParser
from html.entities import name2codepoint


class OpmacHTMLParser(HTMLParser):
    '''
    HTML parser class.

    Passes result to OpmacWriter object specified during initialization.
    '''
    MAX_LINE_LENGTH = 90

    def __init__(self, writer):
        HTMLParser.__init__(self)
        self.writer = writer
        self.convert_charrefs = False

    def handle_starttag(self, tag, attrs):
        '''Encountered opening tag during parsing.'''

        if tag == 'strong' or tag == 'b':
            self.writer.set_bold(True)
        elif tag == 'em' or tag == 'i':
            self.writer.set_italics(True)
        elif tag == 'p' or tag == 'div':
            self.writer.paragraph_start()
        elif tag == 'br':
            self.writer.line_break()
        elif tag == 'img':
            self.writer.image(dict(attrs))
        elif tag == 'a':
            self.writer.link_start(dict(attrs))
        elif tag == 'ol':
            self.writer.ordered_list_start()
        elif tag == 'ul':
            self.writer.bullet_list_start()
        elif tag == 'li':
            self.writer.list_item_start()
        elif tag == 'h1':
            self.writer.chapter()
        elif tag == 'h2':
            self.writer.section()
        elif tag == 'h3':
            self.writer.subsection()
        elif tag == 'h4':
            self.writer.paragraph_start()
            self.writer.set_bold(True)
        elif tag == 'h5':
            self.writer.paragraph_start()
            self.writer.set_italics(True)
        elif tag == 'h6':
            self.writer.paragraph_start()
        elif tag == 'pre':
            self.writer.verbatim_start()
        elif tag == 'blockquote':
            self.writer.blockquote_start()
        elif tag == 'code' or tag == 'kbd':
            self.writer.code_start()
        elif tag == 'hr':
            self.writer.hrule()
        elif tag == 'span':
            pass
        else:
            warn('Start tag not supported: ' + tag)

    def handle_endtag(self, tag):
        '''Encountered closing tag during parsing.'''

        if tag == 'strong' or tag == 'b':
            self.writer.set_bold(False)
        elif tag == 'em' or tag == 'i':
            self.writer.set_italics(False)
        elif tag == 'p' or tag == 'div':
            self.writer.paragraph_end()
        elif tag == 'img' or tag == 'br' or tag == 'hr':
            pass
        elif tag == 'a':
            self.writer.link_end()
        elif tag == 'ol':
            self.writer.list_end()
        elif tag == 'ul':
            self.writer.list_end()
        elif tag == 'li':
            self.writer.list_item_end()
        elif tag == 'h1' or tag == 'h2' or tag == 'h3' or tag == 'h6':
            self.writer.line_break()
        elif tag == 'h4':
            self.writer.set_bold(False)
            self.writer.paragraph_end()
        elif tag == 'h5':
            self.writer.set_italics(False)
            self.writer.paragraph_end()
        elif tag == 'pre':
            self.writer.verbatim_end()
        elif tag == 'blockquote':
            self.writer.blockquote_end()
        elif tag == 'code' or tag == 'kbd':
            self.writer.code_end()
        elif tag == 'span':
            pass
        else:
            warn('End tag not supported: ' + tag)

    def handle_entityref(self, name):
        char_code = name2codepoint[name]
        if char_code == 160:
            self.writer.nobreak_space()
        else:
            self.writer.write_data(chr(char_code))

    def handle_data(self, data):
        '''Encountered raw data during parsing.'''
        self.writer.write_data(data)

    def handle_comment(self, data):
        '''Encountered comment during parsing.'''
        if len(data) > self.MAX_LINE_LENGTH:
            self.writer.block_comment(data)
        else:
            self.writer.inline_comment(data)


class OpmacWriter():
    '''
    Class that accepts parsed HTML data from HTMLParser and converts them
    into OPmac TeX format. Resulting text is written to output stream specified
    during initialization.
    '''

    def __init__(self, output, opts):
        '''
        output - output stream to write converted OPmac text
        opts   - OPmac header options, specified on command line
        '''
        self.out = output
        self.build_escape_table()
        self.text = { 'bold': 0, 'italics': 0, 'verbatim': False }
        self.opts = { 'lang': opts.lang, 'toc': opts.toc, 'nonum': opts.nonum }
        self.list_item = False
        self.code_mode = False
        self.link_desc = False
        self.write_buffer = []

    def build_escape_table(self):
        self.escape_table = str.maketrans({ '\\': r'\\',
                                            '{': r'\{',
                                            '}': r'\}',
                                            '$': r'\$',
                                            '%': r'\%',
                                            '&': r'\&',
                                            '#': r'\#',
                                            '~': r'\~' })

    def escape(self, text):
        'Escape special TeX characters.'
        escaped = text.translate(self.escape_table)
        return escaped

    def set_bold(self, flag):
        if flag:
            self.text['bold'] += 1
        elif self.text['bold'] <= 0:
            warn('Unbalanced bold text')
        else:
            self.text['bold'] -= 1

    def set_italics(self, flag):
        if flag:
            self.text['italics'] += 1
        elif self.text['italics'] <= 0:
            warn('Unbalanced italics text')
        else:
            self.text['italics'] -= 1

    def set_verbatim(self, flag):
        self.text['verbatim'] = flag

    def write_header(self):
        self.out.write('\\input opmac\n')
        self.write_lang()
        self.out.write('\\hyperlinks \\Blue\\Blue\n')       # activate hyperlinks
        self.out.write('\\activettchar"\n')                 # set inline verbatim character
        self.out.write('\\let\\firstnoindent=\\relax\n')    # same indent of paragraphs
        if self.opts['nonum']:                              # no sections numbers, toggle option
            self.out.write('\\nonum \\def\\nonumfalse{\\pagedepth=\\pagedepth}')
        self.line_break()
        if self.opts['toc']:                                # print table of contents, toggle
            self.write_toc()
            self.line_break()

    def write_lang(self):
        if not self.opts['lang'] or self.opts['lang'] == 'cz':
            self.out.write('\\chyph\n')
        elif self.opts['lang'] == 'sk':
            self.out.write('\\shyph\n')
        elif self.opts['lang'] == 'en':
            self.out.write('\\ehyph\n')
        else:
            warn('Invalid language option: ' + self.opts['lang'])

    def write_toc(self):
        self.out.write('\\nonum\\notoc\\sec ')
        if not self.opts['lang'] or self.opts['lang'] == 'cz' or \
           self.opts['lang'] == 'sk':
            self.out.write('Obsah\n')
        elif self.opts['lang'] == 'en':
            self.out.write('Contents\n')
        self.out.write('\\maketoc\n')

    def write_footer(self):
        self.out.write('\n\\end\n')

    def write_data(self, text):
        if text.strip().startswith('```'):
            if text.strip().endswith('```'):
                self.write_block_verbatim(text.strip('```'))
                return
            else:
                self.text['verbatim'] = True
                self.write_buffer.append(text[3:])
                return
        elif text.strip().endswith('```'):
            self.write_buffer.append(text[:-3])
            verbatim_text = ''.join(self.write_buffer)
            self.write_block_verbatim(verbatim_text)
            self.write_buffer.clear()
            self.text['verbatim'] = False
            return
        elif self.text['verbatim']:
            self.write_buffer.append(text)
            return
        elif self.code_mode:
            self.write_inline_verbatim(text)
            return
        else:
            text = text.strip('\n')
            text = self.escape(text)

        if self.link_desc:
            text = text.replace('_', '\_')

        if self.text['italics'] and self.text['bold']:
            self.out.write('{ \\bi ' + text + ' }')
        elif self.text['italics']:
            self.out.write('{ \\it ' + text + ' }')
        elif self.text['bold']:
            self.out.write('{ \\bf ' + text + ' }')
        elif self.list_item:
            text = text.replace('*', "\\char`\\*")
            self.out.write(text)
        else:
            self.out.write(text)

    def write_starttag(self, tag):
        self.out.write('<' + tag + '>')

    def write_endtag(self, tag):
        self.out.write('</' + tag + '>')

    def nobreak_space(self):
        self.out.write('~')

    def line_break(self):
        self.out.write('\n')

    def paragraph_start(self, attrs=None):
        if not self.list_item:      # List defines output text format
            self.line_break()

    def paragraph_end(self):
        if not self.list_item:      # List defines output text format
            self.line_break()

    def chapter(self, attrs=None):
        self.out.write('\n\\chap ')

    def section(self, attrs=None):
        self.out.write('\n\\sec ')

    def subsection(self, attrs=None):
        self.out.write('\n\\secc ')

    def image(self, attrs):
        self.out.write('\\inspic ' + attrs['src'] + '\n')

    def link_start(self, attrs):
        self.out.write('\\ulink[' + attrs['href'] + ']{')
        self.link_desc = True

    def link_end(self):
        self.out.write('}')
        self.link_desc = False

    def hrule(self):
        self.out.write('\n\\vskip 0.5em\\hrule\\vskip 0.5em\n')

    def bullet_list_start(self, attrs=None):
        self.out.write('\n\\begitems\n')

    def list_end(self):
        self.out.write('\\enditems\n')

    def ordered_list_start(self, attrs=None):
        self.out.write('\n\\begitems \\style n\n')

    def list_item_start(self, attrs=None):
        self.list_item = True
        self.out.write('* ')

    def list_item_end(self):
        self.list_item = False
        self.line_break()

    def verbatim_start(self, attrs=None):
        self.text['verbatim'] = True

    def verbatim_end(self):
        text = ''.join(self.write_buffer)
        self.write_block_verbatim(text)
        self.write_buffer.clear()
        self.text['verbatim'] = False

    def write_block_verbatim(self, text):
        self.out.write('\n\\begtt\n')
        self.out.write(text)
        self.out.write('\n\\endtt\n')

    def write_inline_verbatim(self, text):
        self.out.write('"' + text + '"')

    def blockquote_start(self, attrs=None):
        self.set_italics(True)
        self.paragraph_start()

    def blockquote_end(self):
        self.set_italics(False)
        self.paragraph_end()

    def code_start(self, attrs=None):
        # TODO lang
        self.code_mode = True

    def code_end(self):
        self.code_mode = False

    def inline_comment(self, text):
        self.out.write('% ' + text + '\n')

    def block_comment(self, text):
        self.out.write('\n\\iffalse\n')
        self.out.write(text)
        self.out.write('\n\\fi\n')


def warn(message):
    sys.stderr.write('WARNING: ' + message + '\n')


def error(message):
    sys.stderr.write('ERROR: ' + message + '\n')


def parse_markdown(markdown_text):
    """
    Use markdown2 library to parse input markdown text
    """
    try:
        import markdown2
    except ImportError:
        error('Missing python module "markdown2"')
        sys.exit(1)

    parser = markdown2.Markdown()
    html_text = parser.convert(markdown_text)
    return html_text


def read_file(filename):
    try:
        return open(filename, 'r').read()
    except IOError:
        error('Cannot read input file')
        sys.exit(1)


def parse_input_file(filename):
    """
    Parse input file into html text data.
    If input format is markdown, convert to html.
    """
    if filename.endswith('.html'):
        data = read_file(filename)
    else:
        # If file does not have .html extenstion, markdown format is expected
        md_text = read_file(filename)
        data = parse_markdown(md_text)
    return data


def main():
    arg_parser = ArgumentParser()
    arg_parser.add_argument('input_file', nargs='?',
                            help='Input file in Markdown or HTML format')
    arg_parser.add_argument('-o', '--output', nargs='?',
                            help='Output file to write converted OPmac text')
    arg_parser.add_argument('--toc', action="store_true",
                            help='Print table of contents')
    arg_parser.add_argument('--lang',
                            help='Output language settings. Possible values are [cz, sk, en]')
    arg_parser.add_argument('--nonum', action="store_true",
                            help='Do not number chapters and sections')
    args = arg_parser.parse_args()

    if args.input_file:
        data = parse_input_file(args.input_file)
    else:
        # If no input file specified, read input from stdin
        data = sys.stdin.read()

    if args.output:
        output = open(args.output, 'w')
    else:
        output = sys.stdout

    writer = OpmacWriter(output, args)
    parser = OpmacHTMLParser(writer)
    writer.write_header()
    parser.feed(data)
    writer.write_footer()

    output.close()

if __name__ == '__main__':
    main()
