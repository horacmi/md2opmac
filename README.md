# Markdown to OPmac converter

Simple python3 script used to convert from markdown or HTML input format to OPmac TeX format.

Script uses markdown2 library to parse input markdown text. Another option is to use extern
markdown parser and pass parsed HTML text as input. 

## Requirements
* python3
* [markdown2 library](https://github.com/trentm/python-markdown2) 

## Usage

```
usage: md2opmac.py [-h] [-o [OUTPUT]] [--toc] [--lang LANG] [--nonum]
                   [input_file]

positional arguments:
  input_file            Input file in Markdown or HTML format

optional arguments:
  -h, --help            show this help message and exit
  -o [OUTPUT], --output [OUTPUT]
                        Output file to write converted OPmac text
  --toc                 Print table of contents
  --lang LANG           Output language settings. Possible values are [cz, sk,
                        en]
  --nonum               Do not number chapters and sections
```

### Usage examples

Read input from test.md file and write output to test.tex file

    md2opmac -o test.tex test.md

Read HTML input from extern markdown parser and write output to stdout

    markdown test.md | md2opmac

## TODO-list

* Setup script to install md2opmac with dependecies
* Github-flavored markdown support
